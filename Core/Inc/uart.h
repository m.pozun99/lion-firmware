#ifndef __UART_H
#define __UART_H
#include "main.h"

void uart_init();
void uart_transmit_data(uint8_t * data, uint16_t length);
void uart_receive_cmd(void);
void uart_look_for_stop(void);

#endif