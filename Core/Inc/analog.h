#ifndef __ANALOG_H
#define __ANALOG_H

#include "main.h"

void analog_init_dacs(void);
void analog_start_dacs(void);



void analog_set_vbiasDac(uint16_t dacValue);
void analog_set_vthreshDac(uint16_t dacValue);
void analog_get_vbiasDac(uint16_t* value);
void analog_get_vthreshDac(uint16_t* value);

void analog_init_dma(void);
void analog_init_adcs(void);
void analog_start_adcs(void);
void analog_stop_adcs(void);


#endif