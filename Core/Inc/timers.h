#ifndef __TIMERS_H
#define __TIMERS_H
#include "main.h"
void timers_init_timers(void);
void timers_start_timers(void);
void timers_start_pwm(void);

void timers_trigger_discarge_manually(void);

void timer_set_trigger_period(uint16_t period);
void timer_us_delay (uint32_t us);

#endif