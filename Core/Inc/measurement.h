#ifndef __MEASUREMENT_H
#define __MEASUREMENT_H
#include "main.h"

#define MEAS_DATA_FIFO_SIZE 32
#define DATA_BUFFER_SIZE 32

#define GLOBAL_STATUS_IDLE 0
#define GLOBAL_STATUS_MEASURING 1

void meas_start_measurement(void);

void meas_stop_measurement(void);


void meas_init(void);
uint8_t meas_get_status();
void meas_set_status(uint8_t status);
void meas_add_new_data(uint16_t adc_buffers[][DATA_BUFFER_SIZE * 2], uint16_t index);
void meas_send_data();

void meas_set_meas_to_start(uint8_t set);
uint8_t meas_check_for_meas_start(void);
void meas_set_data_transfer(uint8_t set);
uint8_t meas_check_for_data_transfer();

#endif