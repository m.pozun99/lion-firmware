#ifndef __TEMPERATURE_H
#define __TEMPERATURE_H
#include "main.h"
#include "stm32g4xx_hal.h"

float temp_get_temperature();
void temp_convert_temperature();

#endif