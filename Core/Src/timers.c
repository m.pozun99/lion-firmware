#include "timers.h"
#include "temperature.h"
#define   TIM_GLOBAL_PRESCALER 1 // frequency = 40Mhz
#define   TIM_TRIGGER_PERIOD 299 // 10us    // 500 n
#define   TIM_TRIGGER_COMPARE 39 // 2us     // 500 n
#define   TIM_DISCHARGE_PERIOD 189 // 5us   // 1u
#define   TIM_DISCHARGE_PULSE 139 // 4us    // 500 n

#define TIM_TEMP_PRESCALER 39999 // 2kHz
#define TIM_TEMP_PERIOD 20000 // 10s




/*
#define   TIM3_PRESCALER 16000
#define   TIM3_PERIOD 1000
#define  TIM2_PRESCALER 16
#define  TIM2_PERIOD 10*/
#define   TIM16_PRESCALER 799
#define   TIM16_PERIOD 99
#define   TIM16_PULSE 50
#define  TIM_INTER_PERIOD 9
/*
#define   TIM3_PERIOD 1000
#define  TIM2_PRESCALER 16*/

// TIM2 - trigger timertimersr

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim16;
TIM_HandleTypeDef htim17;


static void MX_TIM2_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM6_Init(void);

static void MX_TIM16_Init(void);
static void MX_TIM17_Init(void);



uint32_t timDiscPeriod = TIM_DISCHARGE_PERIOD;
uint32_t timDiscPulse = TIM_DISCHARGE_PULSE;
uint32_t timTrigPeriod = TIM_TRIGGER_PERIOD;
uint32_t timInterPeriod = TIM_INTER_PERIOD;
uint32_t compareTrigValue = TIM_TRIGGER_COMPARE;

uint32_t temp = 0;
//uint32_t timer_set_trigger_period = TIM2_PERIOD;
//uint32_t timer_set_trigger_period = TIM2_PERIOD;

void timer_us_delay (uint32_t us)
{
    __HAL_TIM_SET_COUNTER(&htim6,0);
    while ((__HAL_TIM_GET_COUNTER(&htim6))<us);
}

void timer_set_trigger_period(uint16_t period){
   // HAL_TIM_Base_Stop(&htim16);
    (&htim2)->Instance->ARR = period;
 //   (&htim16)->Init.Period = period;   
  //  HAL_TIM_Base_Start(&htim16);
}

void timer_set_discharge_period(uint16_t period){
   // HAL_TIM_Base_Stop(&htim16);
    (&htim5)->Instance->ARR = period;
 //   (&htim16)->Init.Period = period;   
  //  HAL_TIM_Base_Start(&htim16);
}

void timers_trigger_discarge_manually(void)
/*{
    // Ensure TIM3 is not running
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3); // Stop PWM generation on TIM3 if it's running
    HAL_TIM_Base_Stop(&htim2);               // Stop the base timer

    uint32_t originalValue;

    // Assuming TIM2 and Channel 3 are used
    // Save the original value of the CCR register for Channel 3
    originalValue = TIM2->CCR3;

    // Set the CCR register to 0 (either makes the output continuously low or high based on your configuration)
    TIM2->CCR3 = 0;

    //HAL_TIM_Base_Start(&htim2);               // Start the base timer
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3); 
    // Insert a delay
    HAL_Delay(100);  // Delay for 1000 ms or 1 second
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3); 

    // Restore the original value of the CCR register
    TIM2->CCR3 = originalValue;
    // Start TIM3 in One Pulse Mode
    HAL_TIM_Base_Start(&htim2);  
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3); 
             // Start the base timer
    //HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3); // Start PWM generation on TIM3
}*/

{
    // Stop the timer to ensure it doesn't interfere with GPIO manipulation
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3); 
    HAL_TIM_Base_Stop(&htim2);


    // Reconfigure PA2 as a regular output pin
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    // Set the pin high
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);

    // Insert a delay if needed
    HAL_Delay(100); // 1000 ms delay

    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    // Restore the original configuration
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    // Restart the timer
    HAL_TIM_Base_Start(&htim2);  
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
}


void timers_init_timers(void){
  MX_TIM2_Init(); // discharge
  MX_TIM5_Init(); // trigger
  MX_TIM3_Init();
  MX_TIM6_Init(); // us delay
  MX_TIM17_Init(); // temperature measuring

  if (DEBUG){
      MX_TIM16_Init();
  }
}
void timers_start_timers(void){
  HAL_TIM_Base_Start(&htim2);
  HAL_TIM_Base_Start(&htim3);
  HAL_TIM_Base_Start(&htim5);
  HAL_TIM_Base_Start(&htim6);
  //HAL_TIM_Base_Start(&htim16);
  HAL_TIM_Base_Start_IT(&htim17);

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
  temp_convert_temperature();
}


void timers_start_pwm(void){
  //HAL_TIM_PWM_Start(&htim16, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
  if (DEBUG){
      HAL_TIM_PWM_Start(&htim16, TIM_CHANNEL_1);
  }
  //HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);

}


/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = TIM_GLOBAL_PRESCALER;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = timTrigPeriod;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OnePulse_Init(&htim5, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_TRIGGER;
  sSlaveConfig.InputTrigger = TIM_TS_ETRF;
  sSlaveConfig.TriggerPolarity = TIM_TRIGGERPOLARITY_NONINVERTED;
  sSlaveConfig.TriggerPrescaler = TIM_TRIGGERPRESCALER_DIV1;
  sSlaveConfig.TriggerFilter = 0;
  if (HAL_TIM_SlaveConfigSynchro(&htim5, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC1;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  TIM_OC_InitTypeDef sConfigOC = {0};

  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = compareTrigValue;  // Set your compare match value here
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */

static void MX_TIM3_Init(void)
{
    TIM_SlaveConfigTypeDef sSlaveConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = TIM_GLOBAL_PRESCALER;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = timInterPeriod;  // Set as needed
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
    {
        Error_Handler();
    }

    // Configuring TIM3 as a slave timer triggered by TIM5
    sSlaveConfig.SlaveMode = TIM_SLAVEMODE_TRIGGER;
    sSlaveConfig.InputTrigger = TIM_TS_ITR4; // ITR4 assumes TIM5 is triggering TIM3
    sSlaveConfig.TriggerPolarity = TIM_TRIGGERPOLARITY_RISING;
    sSlaveConfig.TriggerPrescaler = TIM_TRIGGERPRESCALER_DIV1;
    sSlaveConfig.TriggerFilter = 0;
    if (HAL_TIM_SlaveConfigSynchro(&htim3, &sSlaveConfig) != HAL_OK)
    {
        Error_Handler();
    }

    // Configure to generate a TRGO on update event
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
        Error_Handler();
    }

    // Start TIM3 in One Pulse Mode
    HAL_TIM_OnePulse_Init(&htim3, TIM_OPMODE_SINGLE);
}



/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = TIM_GLOBAL_PRESCALER;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = timDiscPeriod;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OnePulse_Init(&htim2, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_TRIGGER;
  sSlaveConfig.InputTrigger = TIM_TS_ITR2;
  if (HAL_TIM_SlaveConfigSynchro(&htim2, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = timDiscPulse;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = TIM16_PRESCALER;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = TIM16_PERIOD;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = TIM16_PULSE;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim16, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim16, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */
  HAL_TIM_MspPostInit(&htim16);

}

static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE ETIM_HandleTypeDef htim17;
   * ND TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = TIM_TEMP_PRESCALER;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = TIM_TEMP_PERIOD;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}



static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 79;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 65535;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}
