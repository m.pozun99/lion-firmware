#include "uart.h"
#include "analog.h"
#include "measurement.h"
#define UART_TIMEOUT 200
#define UART_BAUDRATE 921600
#define STATE_WAITING_CMD 0
#define STATE_WAITING_DATA 1
UART_HandleTypeDef huart1;

uint8_t cmd_buffer[4];
uint8_t recv_buffer[64];
uint16_t data_length = 0; 

uint8_t tx_buffer[64];
uint16_t transLength = 0;
uint16_t temp;

volatile uint8_t uart_state = STATE_WAITING_CMD;
volatile uint8_t cmd_reset = 0;

static void MX_USART1_UART_Init(void);
static void process_cmd();
static uint8_t check_for_stop();
static uint8_t check_for_conformation();

void uart_init(void){
    MX_USART1_UART_Init();
}


void uart_transmit_data(uint8_t * data, uint16_t length){
    HAL_UART_Transmit(&huart1, data, length, UART_TIMEOUT);

}

void uart_look_for_stop(){
    HAL_UART_AbortReceive_IT(&huart1);  // Abort any previous receive
    HAL_UART_Receive_IT(&huart1, cmd_buffer, 1);  // Start reception
}

uint8_t uart_receive_conformation(){
    HAL_UART_AbortReceive_IT(&huart1);  // Abort any previous receive
    HAL_UART_Receive_IT(&huart1, cmd_buffer, 1);  // Start reception
}

void uart_receive_cmd(void){
    HAL_UART_AbortReceive_IT(&huart1);  // Abort any previous receive
    HAL_UART_Receive_IT(&huart1, cmd_buffer, 4);  // Start reception
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
    if (huart->Instance == USART1) {  // Check if it is the correct UART instance
        if(meas_get_status() == GLOBAL_STATUS_IDLE){
            if (uart_state == STATE_WAITING_CMD) {
                data_length = *((uint16_t *)cmd_buffer);  // Get the expected data length
                if (data_length != 0) {
                    uart_state = STATE_WAITING_DATA;
                    HAL_UART_Receive_IT(&huart1, recv_buffer, data_length);  // Receive full data
                } 
                else {
                    process_cmd();
                    uart_receive_cmd();
                }
            }
            else if (uart_state == STATE_WAITING_DATA) {
                process_cmd();  
                uart_receive_cmd();
            }
        }
        else if(meas_get_status() == GLOBAL_STATUS_MEASURING){
            if (check_for_stop()){
                meas_set_data_transfer(0);
                meas_stop_measurement();
            }
            else if (check_for_conformation()){
                meas_set_data_transfer(0);
                uart_look_for_stop();
            }
            else{
                uart_look_for_stop();
            }
        }
    }
}


static uint8_t check_for_stop(){
    if(cmd_buffer[0] == '!'){
        return 1;
    }
    else{
        return 0;
    }
}

static uint8_t check_for_conformation(){
   if(cmd_buffer[0] == 1){
        return 1;
    }
    else{
        return 0;
    }
}

void process_cmd(){
    uint8_t IO_cmd_group = cmd_buffer[2];
    uint8_t IO_cmd_id = cmd_buffer[3];
    uint8_t * data = recv_buffer;
    transLength = 0;
    if(IO_cmd_group==0){     // ANALOG
        switch (IO_cmd_id) {
            case 0:// GET_VBIAS_DAC
            	analog_get_vbiasDac((uint16_t*)tx_buffer);
            	transLength = sizeof(uint16_t);
                break;
            case 1:// SET_VBIAS_DAC
            	analog_set_vbiasDac(*((uint16_t*)data));
                break;
            case 2: // GET_VTHRESH_DAC
            	analog_get_vthreshDac((uint16_t*)tx_buffer);
            	transLength = sizeof(uint16_t);
                break;
            case 3:// SET_VTHRESH_DAC
            	analog_set_vthreshDac(*((uint16_t*)data));
                break;
            case 4:// START MEASUREMENT
            	meas_set_meas_to_start(1);
                break;
            case 5:// STOP MEASUREMENT
            	meas_stop_measurement();
                break;
    }
    }
    else if(IO_cmd_group==1){     // DIGITAL
               switch (IO_cmd_id) {
                   case 0:// SET PERIOD
                    break;
           }
     }
    if(transLength == 0){ // SEND 1 as everything is OK to the master
    	tx_buffer[0] = 1;
    	transLength = 1;
    }
    HAL_UART_Transmit_IT(&huart1, tx_buffer, transLength);
    // set state to IDLE
    uart_state = STATE_WAITING_CMD;

}












/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 921600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}