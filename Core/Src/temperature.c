#define DS18B20_PORT GPIOB
#define DS18B20_PIN GPIO_PIN_4

#include "temperature.h"
#include "timers.h"
volatile float Temperature = 0;

static uint8_t ds18b20_reset(void);

void Set_Pin_Output (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

void ds18b20_write_byte(uint8_t data){
  for (int i = 0; i < 8; i++)
  {
    Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);
    HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET); // Start bit
    timer_us_delay(2);  // Wait a little bit
    if (data & (1 << i))  // Write 1
    {
      HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_SET); // Release the line
      timer_us_delay(60);
    }
    else  // Write 0
    {
      timer_us_delay(60);
      HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_SET); // Release the line
    }
    timer_us_delay(10);  // Recovery time
  }
}

uint8_t ds18b20_read_byte(void){
  uint8_t data = 0;
  for (int i = 0; i < 8; i++)
  {
    Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);
    HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET); // Start bit
    timer_us_delay(2);  // Wait a little bit
    Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // Release the line
    timer_us_delay(10);
    if (HAL_GPIO_ReadPin(DS18B20_PORT, DS18B20_PIN))  // Read the data bit
      data |= (1 << i);
    timer_us_delay(50);  // Complete the time slot
  }
  return data;
}





uint8_t DS18B20_Start (void)
{
	uint8_t Response = 0;
	Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);   // set the pin as output
	HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // pull the pin low
	timer_us_delay (480);   // delay according to datasheet

	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);    // set the pin as input
	timer_us_delay (80);    // delay according to datasheet

	if (!(HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))) Response = 1;    // if the pin is low i.e the presence pulse is detected
	else Response = -1;

	timer_us_delay (400); // 480 us delay totally.

	return Response;
}

void DS18B20_Write (uint8_t data)
{
	Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);  // set as output
	HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET); // Start bit
    timer_us_delay(2);  // Wait a little bit
	for (int i=0; i<8; i++)
	{

		if ((data & (1<<i))!=0)  // if the bit is high
		{
			HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_SET); // Release the line
      		timer_us_delay(60);
			// write 1

			/*Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);  // set as output
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_SET);  // pull the pin LOW
			timer_us_delay (1);  // wait for 1 us

			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // set as input
			timer_us_delay (60);  // wait for 60 us*/
		}

		else  // if the bit is low
		{
			// write 0
			timer_us_delay(60);
      		HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_SET); // Release the line
			/*Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_SET);  // pull the pin LOW
			timer_us_delay (60);  // wait for 60 us

			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);*/
		}
		timer_us_delay(10);
	}
}

uint8_t DS18B20_Read (void)
{
	uint8_t value=0;

	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);

	for (int i=0;i<8;i++)
	{
		Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);   // set as output

		HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET);  // pull the data pin LOW
		timer_us_delay (2);  // wait for > 1us
		Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // set as input
		timer_us_delay(10);
		if (HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))  // if the pin is HIGH
		{
			value |= 1<<i;  // read = 1
		}
		timer_us_delay (60);  // wait for 60 us
	}
	return value;
}

void temp_convert_temperature(){
    volatile uint8_t Temp_byte1, Temp_byte2;
    volatile uint16_t TEMP;
    volatile uint8_t Presence = 0;

	Presence = ds18b20_reset();
    Presence = DS18B20_Start ();
    HAL_Delay (1);
    ds18b20_write_byte(0xCC);  // skip ROM
    ds18b20_write_byte(0x44);  // convert t
    HAL_Delay (10);
    Presence = DS18B20_Start ();
    HAL_Delay(1);
    ds18b20_write_byte(0xCC);  // skip ROM
    ds18b20_write_byte(0xBE);  // Read Scratch-pad

    Temp_byte1 = ds18b20_read_byte();
    Temp_byte2 = ds18b20_read_byte();
    TEMP = (Temp_byte2<<8)|Temp_byte1;
    Temperature = (float)TEMP * 0.0625f;
}

float temp_get_temperature(){
	return Temperature;
}

uint8_t ds18b20_reset(void){
  Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);
  HAL_GPIO_WritePin(DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET); // Pull the line low
  timer_us_delay(480);  // Wait for 480µs
  Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // Release the line
  timer_us_delay(80);   // Wait 15-60µs to check the presence pulse

  if (!HAL_GPIO_ReadPin(DS18B20_PORT, DS18B20_PIN))  // DS18B20 pulled the line low
  {
    timer_us_delay(400);  // Wait for the end of the presence pulse
    return 1;  // Device is present
  }
  return 0;  // No device
}
