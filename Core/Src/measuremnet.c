#include "measurement.h"
#include <string.h> // memcpy
#include "analog.h"
#include "uart.h" 
#include "timers.h" 



uint8_t meas_status = GLOBAL_STATUS_IDLE;

// data sturct
typedef struct{
  uint8_t header[2];
  uint16_t dataA[DATA_BUFFER_SIZE];
  uint16_t dataB[DATA_BUFFER_SIZE];
  uint16_t dataC[DATA_BUFFER_SIZE];
  uint16_t dataD[DATA_BUFFER_SIZE];
  uint32_t timeStamp;
} data_struct_t;


// bufers data
data_struct_t meas_data_fifo[MEAS_DATA_FIFO_SIZE] = {0};
uint16_t meas_fifo_head = 0;           // Index of the next write position
uint16_t meas_fifo_tail = 0;           // Index of the next read position
uint16_t meas_fifo_count = 0;          // Number of items currently in the FIFO
uint32_t meas_overflow_count = 0;

volatile uint8_t meas_to_start = 0;
volatile uint8_t meas_data_transfer_in_progress = 0;

static void reset_fifo(void);

void meas_set_meas_to_start(uint8_t set){
    meas_to_start = set;
}

uint8_t meas_check_for_meas_start(){
  return meas_to_start;
}

void meas_set_data_transfer(uint8_t set){
    meas_data_transfer_in_progress = set;
}

uint8_t meas_check_for_data_transfer(){
  return meas_data_transfer_in_progress;
}


void meas_start_measurement(void){
    reset_fifo();
    meas_set_status(GLOBAL_STATUS_MEASURING);
    /*analog_set_vthreshDac(4000);
    HAL_Delay(100);
    analog_set_vthreshDac(400);*/
    timers_trigger_discarge_manually();
    //HAL_Delay(100);
    analog_start_adcs();
    meas_set_data_transfer(0);
    uart_look_for_stop();

}

void meas_stop_measurement(void){
    meas_set_status(GLOBAL_STATUS_IDLE);
    analog_stop_adcs();
    uart_receive_cmd();
}


void meas_init(void){
    // prepare headers for measurement
    uint16_t i;
    for(i = 0; i < MEAS_DATA_FIFO_SIZE; i++){
        meas_data_fifo[i].header[0] = 'd';
        meas_data_fifo[i].header[1] = DATA_BUFFER_SIZE;
    }
}

void reset_fifo(void){
    meas_fifo_head = 0;
    meas_fifo_tail = 0;
    meas_fifo_count = 0;
    meas_overflow_count = 0;
}


uint8_t meas_get_status(){
  return meas_status;
}

void meas_set_status(uint8_t status){
  meas_status = status;
}


void meas_add_new_data(uint16_t adc_buffers[][DATA_BUFFER_SIZE *2], uint16_t index){
    if (meas_fifo_count < MEAS_DATA_FIFO_SIZE) {
      memcpy(&meas_data_fifo[meas_fifo_head].dataA, &adc_buffers[1][index], DATA_BUFFER_SIZE * 2);
      memcpy(&meas_data_fifo[meas_fifo_head].dataB, &adc_buffers[0][index], DATA_BUFFER_SIZE * 2);
      memcpy(&meas_data_fifo[meas_fifo_head].dataC, &adc_buffers[2][index], DATA_BUFFER_SIZE * 2);
      memcpy(&meas_data_fifo[meas_fifo_head].dataD, &adc_buffers[3][index], DATA_BUFFER_SIZE * 2);
      
      meas_fifo_head = (meas_fifo_head + 1) % MEAS_DATA_FIFO_SIZE;
      meas_fifo_count++;
    }else {
      meas_overflow_count += 1;
      // ERROR OVERFLOW OF BUFFER
    }
}

void meas_send_data(){
    if (meas_fifo_count > 0 && !meas_check_for_data_transfer()) {
        meas_set_data_transfer(1);
        uart_transmit_data((uint8_t *) &meas_data_fifo[meas_fifo_tail] , sizeof(data_struct_t));
        meas_fifo_tail = (meas_fifo_tail + 1) % MEAS_DATA_FIFO_SIZE;
        meas_fifo_count--;
    }

}